﻿using System.ComponentModel;

namespace Twit
{
    public struct Symbols
    {
        public const string Add = "+";
        public const string Subtract = "-";
        public const string Multiply = "*";
        public const string Divide = "/";
        public const string Assignment = "<-";
        public const string OpenParenthesis = ":(";
        public const string CloseParenthesis = "):";
        public const string Bracket = "\"";
        public const string Or = "|";
        public const string And = "&";
        public const string Greater = ">";
        public const string Less = "<";
        public const string GreaterEqual = ">=";
        public const string LessEqual = "<=";
        public const string TurnaryIf = "?";
        public const string TurnaryElse = "!";
        public const string StringWrapper = @"'";
        public const string Member = "#";
        public const string Type = "@";
        public const string Address = "http://";
        public const string Static = "*#";
        public const string Public = ".#";
        public const char Splitter = '.';
    }

    public struct Keywords
    {
        public const string Construct = "FOLLOW";
        public const string Destruct = "UNFOLLOW";
        public const string Output = "TWEET";
        public const string Input = "MESSAGE";
        public const string Loop = "RT";
        public const string Goto = "LINK";
    }

    public struct CompilerData
    {
        public const int MaxSpam = 5;
        public const int MinLineLength = 20;
    }

    public enum TokenType
    {
        Variable, Method, Ignored, Text, MethodCall, VariableModification, Loop, Assignment, Operator, Conditional, Address
    }

    public enum VariableType
    {
        @Public, @Static, @Private
    }

    public enum SentenceType
    {
        Declaration, Calling, Loop, FunctionDeclaration, Modification, Conditional, Address, Link, Empty
    }

}