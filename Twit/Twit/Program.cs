﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twit
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Start(args);
            }
            catch (Exception exception)
            {
                Console.Beep(200, 1000);
                Write("Build failed.");
                Write("Reason: " + exception.Message);
            }
        }

        static void BuildAndCompile(Scanner scanned, string name)
        {
            Write("Scanning file(s)...");
            var files = scanned.Scan();
            Write(scanned.Count + " files scanned, now parsing...");
            var parser = new Parser(files);
            parser.Parse();
            Write("Parsing finished, generating executable...");
            var generator = new Generator(name, parser.Sentences);
            Write(name + ".exe generated. Finished.");
        }

        static void Start(string[] args)
        {
            string name = args[0].EndsWith(".feed") ? args[0].Replace(".feed", "") : args[0];
            if (!String.IsNullOrEmpty(name))
            {
                var scan = new Scanner(name);
                if (scan.IsValid())
                {
                    BuildAndCompile(scan, name);
                }
                else
                {
                    throw new Exception("Unknown error occurred. Exiting.");
                }
            }
            else throw new Exception("No arguments supplied for the compiler.");
        }

        static void Write(string message)
        {
            Console.WriteLine("[Twit] " + message);
        }
    }
}