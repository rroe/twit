﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Security;
using System.Security.Permissions;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CSharp;

namespace Twit
{
    public class Generator
    {
        private IList<Sentence> _sentences; 
        private IList<String> _associatedUsing;
        private IList<String> _associatedCode;
        private Dictionary<String, String> _methods;
        private string _simpleName;

        public Generator(String programName, IList<Sentence> sentences)
        {
            _sentences = sentences;
            _associatedUsing = new List<string>();
            _associatedCode = new List<string>();
            _methods = new Dictionary<string, string>();
            _simpleName = Path.GetFileNameWithoutExtension(programName);
            GenerateAssembly();
            SaveAsFileAndCompile();
        }

        private void SaveAsFileAndCompile()
        {
            using (var file = new System.IO.StreamWriter(_simpleName + ".cs"))
            {
                foreach (string line in _associatedUsing)
                {
                    file.WriteLine(line);
                }
                file.WriteLine("");
                file.WriteLine("namespace Program ");
                file.WriteLine("{");
                file.WriteLine("public class " + _simpleName);
                file.WriteLine("{");
                
                foreach (string line in _associatedCode)
                {
                    file.WriteLine(line);
                }
                file.WriteLine(" ");
                
                file.WriteLine("}");
                file.WriteLine("}");
                file.WriteLine("}");
                file.Close();
                Compile();
            }
        }

        private void Compile()
        {
            var parameters = new CompilerParameters();
            parameters.GenerateExecutable = true;
            parameters.IncludeDebugInformation = true;
            parameters.GenerateInMemory = false;
            parameters.TreatWarningsAsErrors = true;
            parameters.WarningLevel = 3;
            parameters.CompilerOptions = "/optimize";
            parameters.OutputAssembly = _simpleName + ".exe";
            var codeProvider = new CSharpCodeProvider();
            var results = codeProvider.CompileAssemblyFromFile(parameters, new string[] { _simpleName + ".cs" });
            if (results.Errors.HasErrors)
            {
                Console.WriteLine("Unable to compile. Errors:");
                var errors = from e in results.Errors.Cast<CompilerError>()
                               where !e.IsWarning
                               select e;
                foreach (var error in errors)
                {
                    Console.WriteLine("Error: " + error.ErrorText);
                }
            }
            File.Delete(_simpleName + ".cs");
            File.Delete(_simpleName + ".pdb");
        }

        private void GenerateAssembly()
        {
            _associatedUsing.Add("using System;");
            
            _associatedCode.Add("static void Main (String[] args)");
            _associatedCode.Add("{");
            foreach (var sentence in _sentences)
            {
                //Console.WriteLine("[Debug] Generating for sentence: " + BuildStringFromSentence(sentence.WordList, 0));
                GenerateAssemblyForSentence(sentence);
            }
        }

        private void GenerateAssemblyForSentence(Sentence sentence)
        {
            if (sentence.Type == SentenceType.Declaration)
            {
                if (sentence.Tokens[2] == TokenType.MethodCall)
                {
                    //TODO: The TryParse means we can get a string and not just an Int... work on that.
                    string name = BuildVarName(sentence.WordList[0]);
                    string nameImp = "_implicit_" + name;
                    _associatedCode.Add("int " + name + ";");
                    _associatedCode.Add("int " + nameImp + ";");
                    _associatedCode.Add("if (Int32.TryParse(Console.ReadLine(), out " + nameImp + ")) {");
                    _associatedCode.Add(name + " = " + nameImp + ";");
                    _associatedCode.Add("} else { " + name + " = -1; }");
                }
                else
                {
                    _associatedCode.Add("var " + BuildVarName(sentence.WordList[0])
                    + " = " + (sentence.WordList[2].StartsWith(@"'") ? BuildStringFromSentence(sentence.WordList)
                                                                        : BuildVarName(sentence.WordList[2])) + ";");   
                }
            }
            else if (sentence.Type == SentenceType.Conditional)
            {
                //If it got here, then everything is in the right place. Woot, no checking. 
                //I hope they spelled those function names right...
                int length = sentence.WordList.Count;
                if (length == 3)
                {
                    // Any number other than 0 is true. 0 is false. Let's not reinvent the ENTIRE wheel.
                    _associatedCode.Add("if ( " + BuildVarName(sentence.WordList[0]) + " != 0) ");
                    _associatedCode.Add("{");
                    if (sentence.Tokens[2] == TokenType.Address)
                    {
                        _associatedCode.Add("goto " + BuildVarNameFromLink(sentence.WordList[2]) + ";");
                    }
                    else
                    {
                        if (_methods.ContainsKey(BuildVarName(sentence.WordList[2])))
                        {
                            _associatedCode.Add(_methods[BuildVarName(sentence.WordList[2])]);
                        }
                        else
                        {
                            //Because of this, people can call functions wrong and write comments!
                            _associatedCode.Add("//Unable to call method: " + BuildVarName(sentence.WordList[2]));
                        }   
                    }
                    _associatedCode.Add("}");
                }
                else
                {
                    _associatedCode.Add("if ( " + BuildVarName(sentence.WordList[0]) + " != 0) ");
                    _associatedCode.Add("{");
                    if (sentence.Tokens[2] == TokenType.Address)
                    {
                        _associatedCode.Add("goto " + BuildVarNameFromLink(sentence.WordList[2]) + ";");
                    }
                    else
                    {
                        if (_methods.ContainsKey(BuildVarName(sentence.WordList[2])))
                        {
                            _associatedCode.Add(_methods[BuildVarName(sentence.WordList[2])]);
                        }
                        else
                        {
                            _associatedCode.Add("//Unable to call method: " + BuildVarName(sentence.WordList[2]));
                        }                        
                    }
                    _associatedCode.Add("}");
                    _associatedCode.Add("else {");
                    if (sentence.Tokens[4] == TokenType.Address)
                    {
                        _associatedCode.Add("goto " + BuildVarNameFromLink(sentence.WordList[4]) + ";");
                    }
                    else
                    {
                        if (_methods.ContainsKey(BuildVarName(sentence.WordList[4])))
                        {
                            _associatedCode.Add(_methods[BuildVarName(sentence.WordList[4])]);
                        }
                        else
                        {
                            _associatedCode.Add("//Unable to call method: " + BuildVarName(sentence.WordList[4]));
                        }
                    }
                    _associatedCode.Add("}");
                }
            }
            else if (sentence.Type == SentenceType.Address)
            {
                _associatedCode.Add(BuildVarNameFromLink(sentence.WordList[0]) + ":");
                _associatedCode.Add(";");
            }
            else if (sentence.Type == SentenceType.Link)
            {
                // Again, let's just assume the Parser did it's job. Who care's about safety?
                _associatedCode.Add("goto " + BuildVarNameFromLink(sentence.WordList[1]) + ";");
            }
            else if (sentence.Type == SentenceType.Calling)
            {
                if (sentence.WordList[0] == Keywords.Output)
                {
                    // Technically they can only tweet a variable. So... redundant, but whatever.
                    // Still making things up as we go.
                    if (sentence.Tokens[1] == TokenType.Variable)
                    {
                        _associatedCode.Add("Console.Write(" + BuildVarName(sentence.WordList[1]) + ");");
                    }
                }
                else
                {
                    //Calling a usermade method
                    if (_methods.ContainsKey(BuildVarName(sentence.WordList[0])))
                    {
                        _associatedCode.Add(_methods[BuildVarName(sentence.WordList[0])]);
                    }
                    else
                    {
                        _associatedCode.Add("//Unable to call method: " + BuildVarName(sentence.WordList[0]));
                    }
                }

            }
            else if (sentence.Type == SentenceType.Loop)
            {
                _associatedCode.Add("for (int pos = 0; pos < " + sentence.WordList[1] + "; pos++)");
                _associatedCode.Add("{");
                if (_methods.ContainsKey(BuildVarName(sentence.WordList[2])))
                {
                    _associatedCode.Add(_methods[BuildVarName(sentence.WordList[2])]);
                }
                else
                {
                    _associatedCode.Add("//Unable to call method: " + BuildVarName(sentence.WordList[2]));
                }
                _associatedCode.Add("}");
            }
            else if (sentence.Type == SentenceType.FunctionDeclaration)
            {
                //Was gonna need this to accept args, but now we don't need them. Uh... just gonna leave it.
                string builtMethod = BuildVarName(sentence.WordList[0]);
                if (sentence.WordList[2] == Symbols.CloseParenthesis)
                {
                    //Empty arg delcaration, just build

                    for (int pos = 3; pos < sentence.Tokens.Count; pos++)
                    {
                        if (sentence.Tokens[pos] == TokenType.VariableModification)
                        {
                            if (sentence.Tokens[pos + 1] == TokenType.Operator)
                            {
                                if (sentence.Tokens[pos + 2] == TokenType.Variable)
                                {
                                    string var = BuildVarName(sentence.WordList[pos]);
                                    string next = sentence.WordList[pos + 1] + "=";
                                    string addended = BuildVarName(sentence.WordList[pos + 2]);
                                    string line = var + " " + next + " " + addended + ";";
                                    _methods.Add(builtMethod, line);
                                }
                                else
                                {
                                    string var = BuildVarName(sentence.WordList[pos]);
                                    string next = sentence.WordList[pos + 1] + "=";
                                    string addended = BuildStringFromSentence(sentence.WordList, 5);
                                    string line = var + " " + next + " " + addended + ";";
                                    _methods.Add(builtMethod, line);
                                }
                            }
                        }
                    }
                }
            }
        }

        private static string BuildStringFromSentence(List<string> words, int skip = 2)
        {
            string sentence = string.Join(" ", words.ToArray().Skip(skip));
            sentence = sentence.Replace("'", "\"");
            return sentence;
        }

        private static string BuildVarName(String word)
        {
            return word.StartsWith(Symbols.Member) ? word.Substring(1) : word;
        }

        private static string BuildVarNameFromLink(String word)
        {
            return word.StartsWith(Symbols.Address) ? word.Replace(Symbols.Address, "") : word;
        }
    }
}
