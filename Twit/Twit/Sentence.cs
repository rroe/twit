﻿using System;
using System.Collections.Generic;

namespace Twit
{
    public struct Sentence
    {
        public SentenceType Type { get; private set; }

        private List<String> _wordList;
        private List<TokenType> _tokensList;

        public Sentence(SentenceType type)
            : this()
        {
            Type = type;
        }

        public List<string> WordList
        {
            get { return _wordList; }
            set { _wordList = value; }
        }

        public List<TokenType> Tokens
        {
            get { return _tokensList; }
            set { _tokensList = value; }
        }

    }
}