﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.DesignerServices;
using System.Text;
using System.Threading.Tasks;

namespace Twit
{
    public class Parser
    {
        private readonly IList<string> _fileNames;

        private List<Sentence> _sentences;

        public Parser(List<string> list)
        {
            _fileNames = list;
            _sentences = new List<Sentence>();
        }

        public List<Sentence> Sentences
        {
            get { return _sentences; }
            set { _sentences = value; }
        }

        public void Parse()
        {
            foreach (var fname in _fileNames)
            {
                var lines = File.ReadAllLines(fname + ".feed");

                foreach (var s in lines.Reverse())
                {
                    foreach (var line in s.Split(Symbols.Splitter))
                    {
                        var words = line.Trim().Split(' ');
                        var sentenceType = GetSentenceType(words);
                        var tokens = Tokenize(sentenceType, words);
                        _sentences.Add(new Sentence(sentenceType) { WordList = words.ToList(), Tokens = tokens.ToList() });
                    }
                }
            
            }
        }

        private static SentenceType GetSentenceType(IEnumerable<String> words)
        {
            var enumerable = words as string[] ?? words.ToArray();
            if (String.IsNullOrWhiteSpace(enumerable.ElementAt(0)))
            {
                return SentenceType.Empty;
            }
            if (enumerable.ElementAt(0).StartsWith(Symbols.Member) && enumerable.Count() == 1)
            {
                return SentenceType.Calling;
            }
            if (enumerable.ElementAt(0).StartsWith(Symbols.Address) && enumerable.Count() == 1)
            {
                return SentenceType.Address;
            }
            switch (enumerable.ElementAt(0).ToCharArray()[0].ToString())
            {
                case Symbols.Member:
                case Symbols.Public:
                case Symbols.Static:
                    if (String.IsNullOrWhiteSpace(enumerable[1]))
                        throw new Exception("Empty member declaration.");
                    if (enumerable.ElementAt(1) == Symbols.Assignment)
                        return SentenceType.Declaration;
                    if (enumerable.ElementAt(1) == Symbols.OpenParenthesis)
                        return SentenceType.FunctionDeclaration;
                    if (enumerable.ElementAt(1) == Symbols.Bracket)
                        return SentenceType.Calling;
                    if (enumerable.ElementAt(1) == Symbols.TurnaryIf)
                        return SentenceType.Conditional;
                    if (enumerable.ElementAt(1) == Symbols.Add || enumerable.ElementAt(1) == Symbols.Subtract ||
                        enumerable.ElementAt(1) == Symbols.Multiply || enumerable.ElementAt(1) == Symbols.Divide)
                    {
                        return SentenceType.Modification;
                    }
                    
                    throw new Exception("Attempting to perform invalid operation on member: " + BuildSentence(enumerable));
                default:
                    if (enumerable.ElementAt(0) == Keywords.Loop)
                    {
                        if (String.IsNullOrWhiteSpace(enumerable.ElementAt(1)) || String.IsNullOrWhiteSpace(enumerable.ElementAt(2)))
                            throw new Exception("Loop is missing parameters: " + BuildSentence(enumerable));
                        long num;
                        Int64.TryParse(enumerable.ElementAt(1), out num);
                        if (num <= 0)
                            throw new Exception("Invalid number of loops: " + BuildSentence(enumerable));
                        return SentenceType.Loop;
                    }
                    else if (enumerable.ElementAt(0) == Keywords.Output)
                    {
                        return SentenceType.Calling;
                    }
                    else if (enumerable.ElementAt(0) == Keywords.Goto && enumerable.Count() == 2)
                    {
                        return SentenceType.Link;
                    }
                    throw new Exception("Unidentified Exception: " + BuildSentence(enumerable));
            }
        }

        private static string BuildSentence(IList<string> words)
        {
            return words.Aggregate("", (current, word) => current + word + " ");
        }

        private static IEnumerable<TokenType> Tokenize(SentenceType type, IEnumerable<string> words)
        {
            var tokens = new List<TokenType>();
            var enumerable = words as string[] ?? words.ToArray();
            var word = enumerable[0];
            if ((type == SentenceType.Declaration || type == SentenceType.Modification) && enumerable.ToArray().Length > 2)
            {
                if (word.StartsWith(Symbols.Member) && enumerable[1] == Symbols.Assignment)
                {
                    tokens.Add(TokenType.Variable);
                    tokens.Add(TokenType.Assignment);
                    tokens.Add(enumerable[2] == Keywords.Input ? TokenType.MethodCall : TokenType.Text);
                }
                else if (word.StartsWith(Symbols.Member) &&
                          (enumerable[1] == Symbols.Add || enumerable[1] == Symbols.Subtract ||
                           enumerable[1] == Symbols.Multiply || enumerable[1] == Symbols.Divide))
                {
                    tokens.Add(TokenType.VariableModification);
                    tokens.Add(TokenType.Operator);
                    if (!String.IsNullOrWhiteSpace(enumerable[2]))
                    {
                        if (enumerable[2].StartsWith(Symbols.Member))
                        {
                            tokens.Add(TokenType.Variable);
                        }
                        else
                        {
                            tokens.Add(TokenType.Text);
                        }
                    }
                    else
                    {
                        throw new Exception("Invalid variable modification: " + BuildSentence(enumerable));
                    }
                }
                else throw new Exception("Variable assignment error at variable: " + BuildSentence(enumerable));
            }
            else if (type == SentenceType.Address)
            {
                tokens.Add(TokenType.Address);
            }
            else if (type == SentenceType.Link)
            {
                tokens.Add(TokenType.MethodCall);
                if (enumerable[1].StartsWith(Symbols.Address))
                {
                    tokens.Add(TokenType.Address);
                }else throw new Exception("Error linking at: " + BuildSentence(enumerable));
            }
            else if (type == SentenceType.Conditional)
            {
                if(word.StartsWith(Symbols.Member))
                {
                    tokens.Add(TokenType.Variable);
                    //Only way the sentence type could be here is if this is a ? - so no checking needed
                    tokens.Add(TokenType.Conditional);
                    int length = enumerable.Count();
                    if (length == 3)
                    {
                        if (enumerable[2].StartsWith(Symbols.Member))
                        {
                            tokens.Add(TokenType.MethodCall);
                        }
                        else if(enumerable[2].StartsWith(Symbols.Address))
                        {
                            tokens.Add(TokenType.Address);
                        }
                        else throw new Exception("Attempting to perform invalid conditional at: " + BuildSentence(enumerable));
                    }else if (length == 5)
                    {
                        if (enumerable[2].StartsWith(Symbols.Member))
                        {
                            tokens.Add(TokenType.MethodCall);
                        }
                        else if (enumerable[2].StartsWith(Symbols.Address))
                        {
                            tokens.Add(TokenType.Address);
                        }
                        else throw new Exception("Attempting to perform invalid conditional at: " + BuildSentence(enumerable));

                        if (enumerable[3] == Symbols.TurnaryElse)
                        {
                            tokens.Add(TokenType.Conditional);
                            if (enumerable[4].StartsWith(Symbols.Member))
                            {
                                tokens.Add(TokenType.MethodCall);
                            }
                            else if (enumerable[4].StartsWith(Symbols.Address))
                            {
                                tokens.Add(TokenType.Address);
                            }
                            else throw new Exception("Attempting to perform invalid conditional at: " + BuildSentence(enumerable));
                        }
                        else throw new Exception("Attempting to perform invalid conditional at: " + BuildSentence(enumerable));
                    }
                    else throw new Exception("Invalid conditional statement at: " + BuildSentence(enumerable));
                }
            }
            else if (type == SentenceType.FunctionDeclaration)
            {
                tokens.Add(TokenType.Method);
                if (enumerable[1] != Symbols.OpenParenthesis)
                    throw new Exception("Method declaration missing open parenthesis: " + BuildSentence(enumerable));
                tokens.Add(TokenType.Ignored);
                var i = 2;
                // Kinda useless now that we don't allow function arguments. But what the heck, legacy code!
                for (; i < enumerable.Length; i++)
                {
                    var curWord = enumerable[i];
                    if (curWord == Symbols.CloseParenthesis)
                    {
                        tokens.Add(TokenType.Ignored);
                        break;
                    }
                    if (!String.IsNullOrWhiteSpace(curWord))
                    {
                        tokens.Add(TokenType.Variable);
                    }
                    else tokens.Add(TokenType.Ignored);
                }
                var newSentenceType = GetSentenceType(enumerable.Skip(i + 1));
                tokens.AddRange(Tokenize(newSentenceType, enumerable.Skip(i + 1)));
            }
            else if (type == SentenceType.Calling)
            {
                if (word.StartsWith(Symbols.Member))
                {
                    if (words.Count() == 1)
                    {
                        tokens.Add(TokenType.MethodCall);    
                    }
                    else
                    {
                        tokens.Add(TokenType.MethodCall);
                        if (enumerable[1] != Symbols.Bracket)
                            throw new Exception("Method call missing open bracket: " + BuildSentence(enumerable));
                        tokens.Add(TokenType.Ignored);
                        var i = 2;
                        for (; i < enumerable.Length; i++)
                        {
                            var curWord = enumerable[i];
                            if (curWord == Symbols.Bracket)
                            {
                                tokens.Add(TokenType.Ignored);
                                break;
                            }
                            tokens.Add(TokenType.Variable);
                        }    
                    }
                }
                else
                {
                    if (word == Keywords.Output)
                    {
                        tokens.Add(TokenType.MethodCall);
                        tokens.Add(TokenType.Variable);
                    }
                    else
                    {
                        throw new Exception("Error calling method: " + BuildSentence(enumerable));
                    }
                }
            }
            else if (type == SentenceType.Loop)
            {
                if (word == Keywords.Loop && enumerable.Length == 3 && enumerable[2].StartsWith(Symbols.Member))
                {
                    tokens.Add(TokenType.Loop);
                    tokens.Add(TokenType.Text);
                    tokens.Add(TokenType.MethodCall);
                }
                else
                {
                    throw new Exception("Invalid looping parameters at: " + BuildSentence(enumerable));
                }
            }
            else if (type == SentenceType.Empty)
            {
                //Do nothing, is empty.
            }
            else
            {
                throw new Exception("Unidentified token generation error has occurred at line: " + BuildSentence(enumerable));
            }
            return tokens;
        }
    }
}

