﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twit
{
    public class Scanner
    {

        private string[] _file;
        private string _fileName;

        private List<string> _files;
        private int _fileCount;

        public Scanner(String fileName)
        {
            _fileName = fileName;
            _fileCount = 1;
            _file = File.ReadAllLines(fileName + ".feed");
            _files = new List<string>();
            if (_file.Length <= 0)
            {
                throw new Exception("File " + fileName + " not found or empty.");
            }
            _files.Add(fileName);
        }

        public int Count
        {
            get { return _fileCount; }
        }

        public List<string> Scan()
        {
            InitialScan();
            return _files;
        }

        public bool IsValid()
        {
            return InitialScan();
        }

        private bool InitialScan()
        {
            var curLine = 0;
            var spamCount = 0;
            foreach (var line in _file)
            {
                if (line.Length > 140)
                {
                    throw new Exception("Line length too long, at line: " + curLine + " in user feed " + _fileName);
                }
                if (line.Length < CompilerData.MinLineLength)
                {
                    spamCount ++;
                    if (spamCount > CompilerData.MaxSpam)
                    {
                        throw new Exception("User feed '" + _fileName + "' showing too much spam. Quitting.");
                    }
                }
                else
                {
                    if(spamCount > 0) 
                        spamCount --;
                }
                foreach (var word in line.Split(' '))
                {
                    if (word.StartsWith("@"))
                    {
                        var curObj = BuildFileName(word);
                        var objScanner = new Scanner(curObj);
                        if (objScanner.IsValid())
                        {
                            _fileCount ++;
                            _files.Add(curObj);
                        }
                    }
                }
                curLine++;
            }
            return true;
        }

        private static string BuildFileName(string s)
        {
            if (s.StartsWith("@"))
            {
                return s.Substring(1);
            }
            return s;
        }
    }
}
